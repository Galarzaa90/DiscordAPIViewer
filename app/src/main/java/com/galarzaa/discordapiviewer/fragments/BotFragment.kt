/*
 * Copyright (c) 2018 - Allan Galarza <allan.galarza@gmail.com>
 */

package com.galarzaa.discordapiviewer.fragments


import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.galarzaa.discordapiviewer.DiscordApi
import com.galarzaa.discordapiviewer.R
import com.galarzaa.discordapiviewer.ServiceGenerator
import com.galarzaa.discordapiviewer.Utils
import com.galarzaa.discordapiviewer.adapters.GuildsAdapter
import com.galarzaa.discordapiviewer.models.Guild
import com.galarzaa.discordapiviewer.models.User
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_bot.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class BotFragment : Fragment() {
    val TAG : String = "BotFragment"
    var guilds : ArrayList<Guild> = ArrayList()

    lateinit var rootView : View
    lateinit var guildsAdapter : GuildsAdapter

    var guildsLoaded = false
    var guildsExpanded = false

    private lateinit var token: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Log.e(TAG,"onCreateView")
        rootView = inflater.inflate(R.layout.fragment_bot, container, false)
        token = arguments!!.getString("TOKEN")
        Log.e("TAG",token)

        rootView.guildsContainer.layoutManager = LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false)
        guildsAdapter = GuildsAdapter(context, guilds)
        rootView.guildsContainer.adapter = guildsAdapter

        rootView.guildsHeader.setOnClickListener { v -> toggleGuilds() }

        loadUser(token)
        return rootView
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        val toolbar = activity!!.toolbar
        toolbar!!.title = getString(R.string.user)
    }

    fun toggleGuilds(){
        Log.e(TAG,"toggleGuilds")
        if(guildsExpanded){
            rootView.guildsContainer.visibility = View.GONE
            rootView.guildsProgress.visibility = View.GONE
            rootView.guildsHeader.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,R.drawable.ic_arrow_drop_down_black_24dp,0)
            guildsExpanded = false
        }else{
            if(!guildsLoaded){
                loadGuilds(token)
            }else{
                rootView.guildsContainer.visibility = View.VISIBLE
            }
            rootView.guildsHeader.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,R.drawable.ic_arrow_drop_up_black_24dp,0)
            guildsExpanded = true
        }
    }

    fun loadUser(token: String){
        val call = ServiceGenerator.create(DiscordApi::class.java).getMe("Bot ${token.trim()}")
        call.enqueue(object : Callback<User>{
            override fun onFailure(call: Call<User>?, t: Throwable?) {
                Log.e(TAG,"onFailure",t)
            }

            @SuppressLint("SetTextI18n")
            override fun onResponse(call: Call<User>?, response: Response<User>?) {
                val user : User? = response?.body()
                if(response?.isSuccessful == true && user != null){
                    rootView.userId.text = user.id
                    rootView.userDiscriminator.text = "#${user.discriminator}"
                    rootView.userName.text = user.username
                    rootView.userCreated.text = DateFormat.format(Utils.dateFormat,Utils.getCreatedDate(user.id))
                    Picasso.with(context).load(user.avatarUrl).into(rootView.userAvatar)

                    rootView.progressBar.visibility = View.GONE
                    rootView.userCard.visibility = View.VISIBLE
                    rootView.guildCard.visibility = View.VISIBLE
                }
            }

        })
    }

    fun loadGuilds(token: String){
        rootView.guildsProgress.visibility = View.VISIBLE
        val call = ServiceGenerator.create(DiscordApi::class.java).getGuilds("Bot ${token.trim()}")
        call.enqueue(object : Callback<List<Guild>> {
            override fun onFailure(call: Call<List<Guild>>?, t: Throwable?) {
                Log.e(TAG,"onFailure",t)
            }

            override fun onResponse(call: Call<List<Guild>>?, response: Response<List<Guild>>?) {
                Log.e(TAG, "onResponse")
                val body : List<Guild>? = response?.body()
                if (response?.isSuccessful == true && body != null){
                    guilds.clear()
                    guilds.addAll(body)
                    guildsAdapter.notifyDataSetChanged()

                    guildsLoaded = true
                    rootView.guildsProgress.visibility = View.GONE
                    rootView.guildsContainer.visibility = View.VISIBLE
                }
            }

        })
    }

    companion object {
        fun newInstance(token: String): BotFragment {
            val fragment = BotFragment()
            val args = Bundle()
            args.putString("TOKEN",token)
            fragment.arguments = args
            return fragment
        }
    }
}
