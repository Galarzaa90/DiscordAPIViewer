/*
 * Copyright (c) 2018 - Allan Galarza <allan.galarza@gmail.com>
 */

package com.galarzaa.discordapiviewer.fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.afollestad.materialdialogs.MaterialDialog
import com.galarzaa.discordapiviewer.R
import com.galarzaa.discordapiviewer.adapters.TokensAdapter
import com.galarzaa.discordapiviewer.models.Token
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_add_token.view.*
import kotlinx.android.synthetic.main.fragment_tokens.view.*
import java.util.*


class TokenFragment : Fragment() {
    private val TAG = "TokenFragment"
    private lateinit var pref: SharedPreferences
    private val tokenList = ArrayList<Token>()
    private lateinit var adapter : TokensAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pref = activity!!.getSharedPreferences("com.galarzaa.discordapiviewer", Context.MODE_PRIVATE)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView : View = inflater.inflate(R.layout.fragment_tokens, container, false)
        adapter = TokensAdapter(context!!, tokenList)
        adapter.callback = callback
        rootView.tokensContainer.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rootView.tokensContainer.adapter = adapter

        rootView.floatingActionButton.setOnClickListener { v -> addToken(v) }

        val savedTokensJson : String? = pref.getString("tokens", null)
        if(savedTokensJson != null){
            val savedTokens = Gson().fromJson<ArrayList<Token>>(savedTokensJson,
                    object : TypeToken<ArrayList<Token>>(){}.type)
            tokenList.clear()
            tokenList.addAll(savedTokens)
            adapter.notifyDataSetChanged()
        }
        return rootView
    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        val toolbar = activity!!.toolbar
        toolbar!!.title = getString(R.string.tokens)
    }

    private fun addToken(v : View){
        MaterialDialog.Builder(context!!)
                .title(R.string.add_token)
                .customView(R.layout.dialog_add_token,false)
                .positiveText(android.R.string.ok)
                .negativeText(android.R.string.cancel)
                .onPositive { dialog, which ->
                    val customView : View = dialog.customView ?: return@onPositive
                    val tokenContent : String = customView.fieldToken.text.toString()
                    val alias : String = customView.fieldAlias.text.toString()
                    val token = Token(tokenContent,alias)
                    tokenList.add(token)
                    val editor = pref.edit()
                    editor.putString("tokens",Gson().toJson(tokenList))
                    editor.apply()
                    adapter.notifyItemInserted(tokenList.indexOf(token)-1)
                }
                .build().show()
    }

    val callback : TokensAdapter.AdapterCallback = object : TokensAdapter.AdapterCallback{
        override fun onItemSelected(v: View, token: Token) {
            val fragment = BotFragment.newInstance(token.value)
            fragmentManager!!.beginTransaction()
                    .replace(R.id.container,fragment)
                    .addToBackStack(null)
                    .commit()
        }
    }
}