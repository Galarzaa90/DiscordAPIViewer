/*
 * Copyright (c) 2018 - Allan Galarza <allan.galarza@gmail.com>
 */

package com.galarzaa.discordapiviewer

import com.galarzaa.discordapiviewer.models.*
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query


interface DiscordApi {
    @GET("users/@me/guilds")
    fun getGuilds(@Header("Authorization") token: String): Call<List<Guild>>

    @GET("guilds/{guildId}")
    fun getGuild(@Header("Authorization") token: String,
                 @Path("guildId") guildId: String): Call<Guild>

    @GET("guilds/{guildId}/channels")
    fun getGuildChannels(@Header("Authorization") token: String,
                         @Path("guildId") guildId: String): Call<List<Channel>>

    @GET("guilds/{guildId}/members/{userId}")
    fun getGuildMember(@Header("Authorization") token: String,
                       @Path("guildId") guildId: String,
                       @Path("userId") userId : String) : Call<Member>

    @GET("guilds/{guildId}/members")
    fun listGuildMembers(@Header("Authorization") token: String,
                         @Path("guildId") guildId: String,
                         @Query("limit") limit : Int = 1,
                         @Query("after") after : String?): Call<List<Member>>

    @GET("guilds/{guildId}/bans")
    fun getGuildBans(@Header("Authorization") token: String,
                     @Path("guildId") guildId : String) : Call<List<Ban>>

    @GET("guilds/{guildId}/roles")
    fun getGuildRoles(@Header("Authorization") token: String,
                      @Path("guildId") guildId : String) : Call<List<Role>>

    @GET("users/@me")
    fun getMe(@Header("Authorization") token: String) : Call<User>

    @GET("users/@me/channels")
    fun getDMs(@Header("Authorization") token: String) : Call<List<Channel>>
}
