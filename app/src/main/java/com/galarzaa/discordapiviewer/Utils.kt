/*
 * Copyright (c) 2018 - Allan Galarza <allan.galarza@gmail.com>
 */

package com.galarzaa.discordapiviewer

import android.text.format.DateFormat.getBestDateTimePattern
import java.util.*

class Utils(){
    companion object {
        const val DISCORD_EPOCH : Long = 1420070400000

        val dateFormat: String
            get() = getBestDateTimePattern(Locale.getDefault(),"HHmmss zzz MMMMdyyyy")

        fun getCreatedTimestamp(id : String) : Long{
            val numId : Long = id.toLong()
            return ((numId shr 22) + DISCORD_EPOCH) / 1000L
        }

        fun getCreatedDate(id : String) : Date{
            val timestamp = getCreatedTimestamp(id)
            return Date(timestamp*1000L)
        }
    }
}