/*
 * Copyright (c) 2018 - Allan Galarza <allan.galarza@gmail.com>
 */

package com.galarzaa.discordapiviewer.models

data class Overwrite(val id : String, val type : String ,var allow : Int, var Deny : Int){
}