/*
 * Copyright (c) 2018 - Allan Galarza <allan.galarza@gmail.com>
 */

package com.galarzaa.discordapiviewer.models

data class Emoji(val id : String, var name : String){
    var roles : List<String>? = null
    var user : User? = null
    var require_colons : Boolean = true
    var managed : Boolean = false
    var animated : Boolean = false
}