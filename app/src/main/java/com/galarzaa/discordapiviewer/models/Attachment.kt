/*
 * Copyright (c) 2018 - Allan Galarza <allan.galarza@gmail.com>
 */

package com.galarzaa.discordapiviewer.models

data class Attachment(val id : String, val filename : String, val size : Int, val url : String,
                      val proxy_url : String, val height : Int?, val width : Int?)