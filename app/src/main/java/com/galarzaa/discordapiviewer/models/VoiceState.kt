/*
 * Copyright (c) 2018 - Allan Galarza <allan.galarza@gmail.com>
 */

package com.galarzaa.discordapiviewer.models

data class VoiceState(val user_id : String, val channel_id: String, val session_id : String,
                      val deaf : Boolean, val mute : Boolean, val self_deaf : Boolean,
                      val self_mute : Boolean, val supress : Boolean){
    var guild_id : String? = null

}