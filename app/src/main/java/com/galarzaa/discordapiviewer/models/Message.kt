/*
 * Copyright (c) 2018 - Allan Galarza <allan.galarza@gmail.com>
 */

package com.galarzaa.discordapiviewer.models

import java.util.*

/**
 * Represents a message sent in a channel within Discord
 * https://discordapp.com/developers/docs/resources/channel#message-object
 */
data class Message(val id : String, val channel_id : String, val author : User, val content : String,
                   val timestamp : Date?, val edited_timestamp : Date?, val tts : Boolean,
                   val mention_everyone : Boolean, val mentions : List<User>,
                   val mentions_roles : List<Role>, val attachments : List <Attachment>,
                   val embeds : List<Embed>){
    var reactions : List<Reaction>? = null
    var nonce : String? = null
    var pinned : Boolean = false
    var webhook_id : String? = null
    var type : Int = 0

    companion object {
        val DEFAULT = 0
        val RECIPIENT_ADD = 1
        val RECIPIENT_REMOVE = 2
        val CALL = 3
        val CHANNEL_NAME_CHANGE = 4
        val CHANNEL_ICON_CHANGE = 5
        val CHANNEL_PINNED_MESSAGE = 6
        val GUILD_MEMBER_JOIN = 7
    }
}