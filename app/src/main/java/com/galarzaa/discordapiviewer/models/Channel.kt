/*
 * Copyright (c) 2018 - Allan Galarza <allan.galarza@gmail.com>
 */

package com.galarzaa.discordapiviewer.models

data class Channel(val id : String, var type : Int){
    var guild_id : String? = null
    var position : Int = 0
    var permission_overwrites : List<Overwrite>? = null
    var name : String? = null
    var topic : String? = null
    var nsfw : Boolean = false
    var last_message_id : String? = null
    var bitrate : Int = 0
    var user_limit : Int = 0
    var recipients : List<User>? = null
    var icon : String? = null
    var owner_id : String? = null
    var application_id : String? = null
    var parent_id : String? = null
    var last_pin_timestamp : String? = null
}