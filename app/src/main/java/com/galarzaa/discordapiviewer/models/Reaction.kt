/*
 * Copyright (c) 2018 - Allan Galarza <allan.galarza@gmail.com>
 */

package com.galarzaa.discordapiviewer.models

/**
 * https://discordapp.com/developers/docs/resources/channel#reaction-object
 */
data class Reaction(val emoji : Emoji, val count : Int, val me : Boolean){

}