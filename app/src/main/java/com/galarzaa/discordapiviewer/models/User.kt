/*
 * Copyright (c) 2018 - Allan Galarza <allan.galarza@gmail.com>
 */

package com.galarzaa.discordapiviewer.models

data class User(val id : String){
    var username : String? = null
    var discriminator : String? = null
    var avatar : String? = null
    var bot : Boolean = false
    var mfa_enabled : Boolean = false
    var verified : Boolean = false
    var email : String? = null

    val avatarUrl : String
        get() = "https://cdn.discordapp.com/avatars/$id/$avatar.png"

}