/*
 * Copyright (c) 2018 - Allan Galarza <allan.galarza@gmail.com>
 */

package com.galarzaa.discordapiviewer.models

data class Role(var id : String, var name : String, var color : Int, var hoist : Boolean,
                var position : Int, var permissions : Int, var managed : Boolean,
                var mentionable : Boolean){

}