/*
 * Copyright (c) 2018 - Allan Galarza <allan.galarza@gmail.com>
 */

package com.galarzaa.discordapiviewer.models

/**
 * https://discordapp.com/developers/docs/resources/guild#ban-object
 */
data class Ban(val user : User, val reason : String?)