/*
 * Copyright (c) 2018 - Allan Galarza <allan.galarza@gmail.com>
 */

package com.galarzaa.discordapiviewer.models

/**
 * https://discordapp.com/developers/docs/resources/channel#embed-object
 */
data class Embed(val title : String, val description : String){
}