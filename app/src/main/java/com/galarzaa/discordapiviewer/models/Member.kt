/*
 * Copyright (c) 2018 - Allan Galarza <allan.galarza@gmail.com>
 */

package com.galarzaa.discordapiviewer.models

data class Member(val user : User, val roles : List<String>, val deaf : Boolean, val mute : Boolean,
                  val joined_at : String){
    var nick : String? = null
}