/*
 * Copyright (c) 2018 - Allan Galarza <allan.galarza@gmail.com>
 */

package com.galarzaa.discordapiviewer.models

import java.util.*

/**
 *  Guilds in Discord represent an isolated collection of users and channels, and are often referred
 *  to as "servers" in the UI.
 *  https://discordapp.com/developers/docs/resources/guild#guild-object
 */
data class Guild(var id : String, var name : String){
    var icon : String? = null
    var splash : String? = null
    var owner : Boolean = false
    var owner_id : String? = null
    var permissions : Int = 0
    var region : String? = null
    var afk_channel_id : String? = null
    var afk_timeout : Int = 0
    var embed_enabled : Boolean = false
    var embed_channel_id: String? = null
    var verification_level: Int = VERIFICATION_NONE
    var default_message_notifications: Int? = null
    var roles : List<Role>? = null
    var emojis : List<Emoji>? = null
    var features : List<String>? = null
    var mfa_level: Int? = null
    var joined_at: Date? = null
    var large: Boolean? = null
    var unavailable: Boolean? = null
    var member_count: Int? = null
    //List<VoiceState> voice_states;
    var members : List<Member>? = null
    var channels : List<Channel>? = null

    val iconUrl : String
        get() = "https://cdn.discordapp.com/icons/$id/$icon.png"

    companion object {
        val VERIFICATION_NONE = 0
        val VERIFICATION_LOW = 1
        val VERIFICATION_MEDIUM = 2
        val VERIFICATION_HIGH = 3
        val VERIFICATION_VERY_HIGH = 4
    }
}