/*
 * Copyright (c) 2018 - Allan Galarza <allan.galarza@gmail.com>
 */

package com.galarzaa.discordapiviewer.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.galarzaa.discordapiviewer.R
import com.galarzaa.discordapiviewer.Utils
import com.galarzaa.discordapiviewer.models.Guild
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_guild.view.*


class GuildsAdapter(private val context: Context?, private val guilds: ArrayList<Guild>) : RecyclerView.Adapter<GuildsAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.item_guild, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val guild : Guild = guilds[position]
        holder?.name?.text = guild.name
        holder?.id?.text = guild.id
        holder?.created?.text = DateFormat.format(Utils.dateFormat, Utils.getCreatedDate(guild.id))
        Picasso.with(context).load(guild.iconUrl).into(holder?.icon)
    }

    override fun getItemCount(): Int {
        return guilds.size
    }

    inner class ViewHolder(view : View) : RecyclerView.ViewHolder(view){
        val name : TextView = view.guildName
        val icon : ImageView = view.guildIcon
        val id : TextView = view.guildId
        val created : TextView = view.guildCreated
    }
}