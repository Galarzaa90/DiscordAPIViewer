/*
 * Copyright (c) 2018 - Allan Galarza <allan.galarza@gmail.com>
 */

package com.galarzaa.discordapiviewer.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.galarzaa.discordapiviewer.R
import com.galarzaa.discordapiviewer.models.Token
import kotlinx.android.synthetic.main.item_token.view.*
import java.util.*

class TokensAdapter(private val context: Context, private val tokens: ArrayList<Token>) : RecyclerView.Adapter<TokensAdapter.ViewHolder>(){
    val TAG = "TokensAdapter"
    var callback : AdapterCallback? = null
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.item_token, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val token = tokens[position]
        holder.token.text = token.value
        holder.alias.text = token.alias
        holder.container.setOnClickListener { v ->
            callback?.onItemSelected(v,token)
            Log.e("TAG","onItemSelected(v, token = ${token.value}")
        }
    }

    override fun getItemCount(): Int {
        return tokens.size
    }

    inner class ViewHolder(val view : View) : RecyclerView.ViewHolder(view){
        val container : View = view.container
        val token : TextView = view.tokenContent
        val alias : TextView = view.tokenName
    }

    interface AdapterCallback{
        fun onItemSelected(v: View, token: Token)
    }
}