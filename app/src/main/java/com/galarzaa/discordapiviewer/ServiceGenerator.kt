/*
 * Copyright (c) 2018 - Allan Galarza <allan.galarza@gmail.com>
 */

package com.galarzaa.discordapiviewer

import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ServiceGenerator {
    companion object {
        private val builder = Retrofit.Builder()
                .baseUrl("https://discordapp.com/api/v6/")
                .addConverterFactory(GsonConverterFactory.create(Gson()))

        private var retrofit : Retrofit = builder.build()

        private val logging : HttpLoggingInterceptor =
                HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

        fun <S> create(serviceClass: Class<S>): S {
            val httpClient = OkHttpClient.Builder()
            if (!httpClient.interceptors().contains(logging)) {
                httpClient.addInterceptor(logging)
                builder.client(httpClient.build())
                retrofit = builder.build()
            }
            return retrofit.create(serviceClass)
        }

        fun retrofit(): Retrofit {
            return retrofit;
        }
    }
}