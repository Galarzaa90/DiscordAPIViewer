/*
 * Copyright (c) 2018 - Allan Galarza <allan.galarza@gmail.com>
 */

package com.galarzaa.discordapiviewer

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.galarzaa.discordapiviewer.fragments.TokenFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val fragmentManager = supportFragmentManager

        fragmentManager.beginTransaction()
                .replace(R.id.container, TokenFragment())
                .commit()
    }
}
