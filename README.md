# DiscordAPIViewer
An app to explore through API endpoints.

This project is meant to practice Kotlin development. Contributions and suggestions are welcome.

## Screenshots

![image](docs/assets/images/TokensFragment.png)
![image](docs/assets/images/BotFragment_1.png)
![image](docs/assets/images/BotFragment_2.png)
